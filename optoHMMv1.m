function optoHMMv1(variance,K,rep)
%
% Fit a hidden Markov model on mouse fMRI data.
%
% Inputs
%  K: number of HMM states
%  variance: variance explained in fMRI data (through PCA approach)
%  rep: repetition number (only to create output folder)
%
% Author: P.Salvan
% Date: Jan 2020

%% Dependencies
tic
addpath('/<path_to>/FSLNets')   % wherever you've put this package
addpath(sprintf('%s/etc/matlab',getenv('FSLDIR')))    % you don't need to edit this if FSL is setup already

addpath(genpath('/<path_to>/HMM-MAR-master')) % wherever you've put this package

%% Not to start parallel environments
ps = parallel.Settings;
ps.Pool.AutoCreate = false;

%% root
root = '/<main_folder>/opto';
root_data = [root '/data'];
root_results = [root '/results' ];

%% load time-series
load([root_data '/workspace_ts_opto.mat']); % <-- workspace with your favourite time-series
disp('Workspace loaded correctly.')

%% set up HMM parameters
disp('Set up HMM parameters: ...')
% % % data
L = ts.NtimepointsPerSubject ;
N = ts.Nsubjects ;
T = L * ones(1,N);
data = ts.ts ;

% % % fixed options
options.order = 0; % no autoregressive components
options.zeromean = 0; % model the mean
options.covtype = 'full' ;
options.Fs = 1/ts.tr;
options.verbose = 1;
options.standardise = 1;
options.inittype = 'HMM-MAR';
options.cyc = 1000;
options.tol = 1e-8;

% % % mod options
options.K = K ;
options.pca = variance ;
disp([' number of states: ' num2str(options.K)])
disp([' ts variance expl: ' num2str(options.pca)])
disp(' ')

%% run HMM
disp('Computing HMM ...')
[hmm, Gamma, Xi, vpath, ~, residuals] = hmmmar(data,T,options);
disp('Done.')
disp(' ')

%% quantify HMM outputs
disp('Computing free energy and temporal features ...')
%tic
% % Free energy
fe = hmmfe(data,T,hmm,Gamma);
% % Viterbi path
viterbipath = hmmdecode(data,T,hmm,1) ;
% % Mean activity
for k=1:options.K
    meanActivity(k,:) = getMean(hmm, k) ;
end
% % Functional connectivity
for k=1:options.K
    func_conn(:,:,k) = getFuncConn(hmm, k) ;
end
% % Fractional occupancy (& max)
FracOcc = getFractionalOccupancy(Gamma,T) ;
maxFracOcc = max(FracOcc,[],2);
% % Switching rate
switchingRate = getSwitchingRate(Gamma,T);
%toc
disp('Done.')
disp(' ')

%% save HMM outputs
fname = [root_results '/workspace_opto_hmm_variance' num2str(variance) '_K' num2str(K) '_rep' num2str(rep) '.mat'] ;
save(fname,'hmm','Gamma','Xi','vpath','residuals','fe',...
           'viterbipath','meanActivity','func_conn',...
           'FracOcc','maxFracOcc','switchingRate',...
           'deltaT') ;
disp('Workspace saved.')

system(['ls -hl ' fname])
disp(' ')
disp('Total time:')
toc
disp(' ')

end
