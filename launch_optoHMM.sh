#!/bin/sh
root=/<path_to_folder>/opto # folder with all the data
rootLogs=$root/logsHMM
rootData=$root/data
rootScripts=/<path_to_folder>/scripts/optoHMM # folder where this filed is contained
fileCommands=$rootScripts/launch_commands_optoHMM.txt

rm $fileCommands
rm -r $rootLogs
module load MATLAB/2018b


# Set explained variance
for variance in 0.5
do
  # Set number of HMM states
for K in 10 12 14 16 18
  do
    # Set number of repetitions
    for rep in {1..5}
    do
      echo 'matlab -nodisplay -nosplash -r "optoHMMv1('$variance, $K, $rep')" ' >> $fileCommands
    done
  done
done

fsl_sub -q long.q -l $rootLogs -t $fileCommands -s openmp,2 -N "optoHMM"
echo 'All jobs submitted'
echo
