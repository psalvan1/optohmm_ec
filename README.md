# optoHMM_EC
Code related to the publication: "Frequency-modulation of entorhinal cortex neuronal activity drives distinct frequency-dependent states of brain-wide dynamics"

Matlab code to perform Hidden Markov modelling (HMM) of entorhinal cortex optogenetic-fMRI.

Further details on HMM parameters can be found at https://github.com/OHBA-analysis/HMM-MAR.

How to cite this material:
DOI: 10.5281/zenodo.5541144
